package io.itit.smartjdbc;

/**
 * 
 * @author skydu
 *
 */
public class Param {

	public String name;
	
	public Object value;
	
	public Param(String name,Object value) {
		this.name=name;
		this.value=value;
	}
}
